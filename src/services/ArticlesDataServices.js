import http from "../http-common";

class ArticlesDataServices {
  getAll() {
    return http.get("/articles");
  }

  getById(id) {
    return http.get("/article/"+id);
  }

  search(expression) {
    return http.get("/search/"+expression);
  }

}

export default new ArticlesDataServices();