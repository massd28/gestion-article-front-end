import http from "../http-common";

class CategoryDataServices {
  getAll() {
    return http.get("/categories");
  }

  getById(id) {
    return http.get("/categorie/"+id);
  }

}

export default new CategoryDataServices();