import Vue from 'vue'
import VueRouter from 'vue-router'
import ArticleDetails from '../components/ArticleDetails.vue'
import CategoryDetails from '../components/CategoryDetails.vue'
import CategoryList from '../components/CategoryList.vue'
import SearchResult from '../components/SearchResult.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: "/",
    alias: "/categories",
    name: "categories",
    component: (CategoryList)
  },
  {
    path: "/categorie/:id",
    name: "categorie-details",
    component: (CategoryDetails)
  },
  {
    path: "/article/:id",
    name: "article-details",
    component: (ArticleDetails)
  },
  {
    path: "/search/:expression",
    name: "search",
    component: (SearchResult)
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_API_URL,
  routes
})

export default router
