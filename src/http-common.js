import axios from "axios";

const apiRootUrl = process.env.VUE_APP_BASE_URL;

export default axios.create({
  baseURL: apiRootUrl,
  headers: {
    "Content-type": "application/json"
  }
});